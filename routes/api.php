<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Http\Controllers\StoryController;
use App\Http\Controllers\BoardController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/', function(Request $request){
	return "200 Working!";
});

// Create a Board
Route::post('/board', [BoardController::class, 'create']);

// Get Boards
Route::get('/board', [BoardController::class, 'index']);
Route::post('/story/create', [StoryController::class, 'create']);
Route::post('/story/list', [BoardController::class, 'details']);
