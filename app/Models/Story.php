<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Story
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
		'title',
		'priority',
		'points',
		'assignee',
		'status',
		'board_id'
    ];

	protected $table = 'stories';

}
