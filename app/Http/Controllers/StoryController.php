<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;


class StoryController extends BaseController {
	
	private function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
		
	
	function index(Request $request){
		$name = $request->input('name');
		$boards = DB::table('boards')->where('name', $name)->get();
		return $boards;
	}
	
	
	function create(Request $request){
		$title = $request->input('title');
		$priority = $request->input('priority');
		$points = $request->input('points');
		$boardId = $request->input('boardId');
		$storyIndex = $this->generateRandomString(12);
		
		DB::table('stories')->insert([
			'id' => $storyIndex,
		    'title' => $title,
		    'priority' => $priority,
			'points'=>$points,
			'board_id'=>$boardId
		]);
		return $storyIndex;
	}
	
	
}

