<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;


class BoardController extends BaseController {
	
	private function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
		
	
	function index(Request $request){
		$name = $request->input('name');
		$boards = DB::table('boards')->where('name', $name)->get();
		return $boards;
	}
	
	function details(Request $request){
		$name = $request->input('name');
		$board = DB::table('boards')->where('name', $name)->first();
		$boardId = $board->id;
		
		$stories = DB::table('stories')->where('board_id', $boardId)->get();
		
		$board['stories'] = $stories;
		
		return $board;
		
	}
	
	
	// Create a board
	function create(Request $request){
		$name = $request->input('name');
		$boardIndex = $this->generateRandomString(12);
		DB::table('boards')->insert([
		    'id' => $boardIndex,
		    'name' => $name
		]);
		return $boardIndex;
	}
	
	
}

