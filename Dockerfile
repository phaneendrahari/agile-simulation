FROM ubuntu:18.04


RUN mkdir /app
COPY start.sh /app
RUN apt-get update --fix-missing
RUN apt-get install software-properties-common -y
RUN apt-get install nginx -y
RUN LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
RUN apt-get update --fix-missing
RUN apt-get install -y php7.3 php7.3-bcmath php7.3-bz2 php7.3-cgi php7.3-cli php7.3-common php7.3-curl php7.3-dba php7.3-json php7.3-mbstring php7.3-fpm php7.3-xml php7.3-mysql
RUN apt-get install -y php7.3-gd php7.3-zip
RUN apt-get install sudo nano composer curl -y
RUN curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
RUN apt-get install nodejs -y

CMD ["sh","/app/start.sh"]
